import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { LectorCourse } from "./entites/lector.course.entity";

@Module({
  imports: [TypeOrmModule.forFeature([LectorCourse])],
})
export class LectorcourseModule {}
