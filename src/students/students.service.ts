import ObjectID from "bson-objectid";
import { Injectable } from "@nestjs/common";
import { Student } from "./entites/student.entity";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { CreateStudentDto } from "./dto/CreateStudent.dto";
import path = require("path");
import fs = require("fs/promises");
import { UpdateStudentDto } from "./dto/UpdateStudent.dto";
import { ExceptionsHendler } from "src/exceptions/exceptions";
@Injectable()
export class StudentsService {
  constructor(
    @InjectRepository(Student)
    private readonly studentReposetory: Repository<Student>,
  ) {}

  async getAllStudents(): Promise<Student[]> {
    return await this.studentReposetory
      .createQueryBuilder("student")
      .select(["student.id as id", "student.name as name", "student.surname as surname", "student.email as email", "student.age as age"])
      .leftJoin("student.group", "group")
      .addSelect('group.name as "groupName"')
      .getRawMany();
  }

  async getStudentById(id: string): Promise<Student> {
    const student = await this.studentReposetory
      .createQueryBuilder("student")
      .select(["student.id as id", "student.name as name", "student.surname as surname", "student.email as email", "student.age as age"])
      .leftJoin("student.group", "group")
      .addSelect('group.name as "groupName"')
      .where("student.id = :id", { id })
      .getRawOne();

    if (!student) {
      throw new ExceptionsHendler("Student with this id not found", 404);
    }

    return student;
  }

  async getStudentByName(name: string): Promise<Student[]> {
    const students = await this.studentReposetory
      .createQueryBuilder("student")
      .select(["student.id as id", "student.name as name", "student.surname as surname", "student.email as email", "student.age as age"])
      .leftJoin("student.group", "group")
      .addSelect('group.name as "groupName"')
      .where("student.name ILIKE :name", { name: name })
      .getRawMany();

    if (students.length === 0) {
      throw new ExceptionsHendler("Student with this name not found", 404);
    }
    return students;
  }

  async createStudent(createStudentDto: CreateStudentDto): Promise<any> {
    const student = await this.studentReposetory.findOne({
      where: {
        email: createStudentDto.email,
      },
    });

    if (student) {
      throw new ExceptionsHendler("Student with this email already exists", 400);
    }

    return await this.studentReposetory.save(createStudentDto);
  }

  async updateStudent(id: string, updateStudentDto: UpdateStudentDto): Promise<Student> {
    const student = await this.getStudentById(id);
    if (!student) {
      throw new ExceptionsHendler("Student with this id not found", 404);
    }
    await this.studentReposetory.update(id, updateStudentDto);
    return await this.getStudentById(id);
  }

  async setGroupForStudent(studentId: string, groupId: number): Promise<Student> {
    const student = await this.getStudentById(studentId);
    if (!student) {
      throw new ExceptionsHendler("Student with this id not found", 404);
    }

    await this.studentReposetory.createQueryBuilder("studentgroup").update().set({ groupId: groupId }).where("id = :id", { id: studentId }).execute();

    return await this.getStudentById(studentId);
  }

  async addStudentImage(id: string, filePath: string): Promise<Student> {
    const student = await this.getStudentById(id);
    if (!student) {
      throw new ExceptionsHendler("Student with this id not found", 404);
    }
    if (!filePath) {
      throw new ExceptionsHendler("File is not provided", 400);
    }
    try {
      const imageExtension = path.extname(filePath);
      const imageId = ObjectID().toHexString();
      const imageName = imageId + imageExtension;
      const currentPath = __dirname;

      const studentsDirectoryName = "students";
      const parentPath = path.dirname(currentPath);

      const studentsDirectoryPath = path.join(parentPath, "../src/", "public", studentsDirectoryName);

      const newImagePath = path.join(studentsDirectoryPath, imageName);

      await fs.rename(filePath, newImagePath);

      this.studentReposetory.createQueryBuilder("studentgroup").update().set({ imagePath: newImagePath }).where("id = :id", { id }).execute();

      return await this.getStudentById(id);
    } catch (error) {
      await fs.unlink(filePath);
      throw error;
    }
  }

  async deleteStudent(id: string) {
    const student = await this.getStudentById(id);
    if (!student) {
      throw new ExceptionsHendler("Student with this id not found", 404);
    }
    await this.studentReposetory.delete(id);
  }
}
