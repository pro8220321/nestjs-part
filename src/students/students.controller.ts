import { Body, Controller, Get, Param, Post, Query, Patch, Delete, UploadedFile, UseInterceptors, UseGuards } from "@nestjs/common";
import { StudentsService } from "./students.service";
import { CreateStudentDto } from "../students/dto/CreateStudent.dto";
import { FileInterceptor } from "@nestjs/platform-express";
import multerConfig from "../config/middleware/config.multer";
import { UpdateStudentDto } from "./dto/UpdateStudent.dto";
import { SetGroupForStudentDto } from "./dto/SetGroupForStudent.dto";
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiBody,
  ApiConsumes,
  ApiCreatedResponse,
  ApiInternalServerErrorResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiQuery,
  ApiTags,
  ApiUnauthorizedResponse,
} from "@nestjs/swagger";
import { StudentCreateResponse, StudentResponse } from "./types/student-get-response.interface";
import { AuthGuard } from "src/auth/auth.guards";

@ApiTags("Students")
@Controller("api/v1/students")
export class StudentsController {
  constructor(private studentsService: StudentsService) {}

  @Get("")
  @ApiBearerAuth()
  @UseGuards(AuthGuard)
  @ApiQuery({
    name: "name",
    type: String,
    required: false,
    description: "Student name",
  })
  @ApiOperation({ summary: "Get students" })
  @ApiOkResponse({
    description: "Get student by name",
    type: [StudentResponse],
  })
  @ApiNotFoundResponse({ description: "Student with this name not found" })
  @ApiInternalServerErrorResponse({
    description: "Internal Server Error",
  })
  @ApiUnauthorizedResponse({
    description: "Unauthorized exception",
  })
  async getAllStudents(@Query("name") name: string) {
    if (name) {
      return await this.studentsService.getStudentByName(name);
    } else {
      return await this.studentsService.getAllStudents();
    }
  }

  @Get("/:id")
  @ApiBearerAuth()
  @UseGuards(AuthGuard)
  @ApiOperation({ summary: "Get student by id" })
  @ApiOkResponse({
    description: "Get student by id",
    type: StudentResponse,
  })
  @ApiNotFoundResponse({ description: "Student with this id not found" })
  @ApiInternalServerErrorResponse({
    description: "Internal Server Error",
  })
  @ApiUnauthorizedResponse({
    description: "Unauthorized exception",
  })
  async getStudentsById(@Param("id") id: string) {
    return await this.studentsService.getStudentById(id);
  }

  @Post("")
  @ApiBearerAuth()
  @UseGuards(AuthGuard)
  @ApiOperation({ summary: "Create student" })
  @ApiCreatedResponse({
    description: "Create student",
    type: StudentCreateResponse,
  })
  @ApiBadRequestResponse({
    description: "Student with this email already exists",
  })
  @ApiInternalServerErrorResponse({
    description: "Internal Server Error",
  })
  @ApiUnauthorizedResponse({
    description: "Unauthorized exception",
  })
  async createStudent(@Body() createStudentDto: CreateStudentDto) {
    return await this.studentsService.createStudent(createStudentDto);
  }

  @Patch("/:id")
  @ApiBearerAuth()
  @UseGuards(AuthGuard)
  @ApiOperation({ summary: "Update student" })
  @ApiCreatedResponse({
    description: "Update student",
    type: StudentResponse,
  })
  @ApiNotFoundResponse({ description: "Student with this id not found" })
  @ApiInternalServerErrorResponse({
    description: "Internal Server Error",
  })
  @ApiUnauthorizedResponse({
    description: "Unauthorized exception",
  })
  async updateStudent(@Body() updateStudentDto: UpdateStudentDto, @Param("id") id: string) {
    return await this.studentsService.updateStudent(id, updateStudentDto);
  }

  @Patch("/:id/group")
  @ApiBearerAuth()
  @UseGuards(AuthGuard)
  @ApiOperation({ summary: "Set group for student" })
  @ApiOkResponse({
    description: "Set group for student",
    type: StudentResponse,
  })
  @ApiNotFoundResponse({ description: "Student with this id not found" })
  @ApiInternalServerErrorResponse({
    description: "Internal Server Error",
  })
  @ApiUnauthorizedResponse({
    description: "Unauthorized exception",
  })
  async setGroupForStudent(@Body() body: SetGroupForStudentDto, @Param("id") id: string) {
    return await this.studentsService.setGroupForStudent(id, body.groupId);
  }

  @Patch("/:id/image")
  @ApiBearerAuth()
  @UseGuards(AuthGuard)
  @ApiOperation({ summary: "Set image for student" })
  @ApiConsumes("multipart/form-data")
  @ApiBody({
    schema: {
      type: "object",
      properties: {
        file: {
          type: "string",
          format: "binary",
        },
      },
    },
  })
  @ApiOkResponse({
    description: "Set image for student",
    type: StudentResponse,
  })
  @ApiNotFoundResponse({ description: "Student with this id not found" })
  @ApiBadRequestResponse({ description: "File is not provided" })
  @UseInterceptors(FileInterceptor("file", multerConfig))
  @ApiInternalServerErrorResponse({
    description: "Internal Server Error",
  })
  @ApiUnauthorizedResponse({
    description: "Unauthorized exception",
  })
  async addStudentImage(
    @Param("id") id: string,
    @UploadedFile()
    file: Express.Multer.File,
  ) {
    return await this.studentsService.addStudentImage(id, file.path);
  }

  @Delete("/:id")
  @ApiBearerAuth()
  @UseGuards(AuthGuard)
  @ApiOperation({ summary: "Delete student" })
  @ApiOkResponse({ description: "Deleted student" })
  @ApiNotFoundResponse({ description: "Student with this id not found" })
  @ApiInternalServerErrorResponse({
    description: "Internal Server Error",
  })
  @ApiUnauthorizedResponse({
    description: "Unauthorized exception",
  })
  async deleteStudent(@Param("id") id: string) {
    await this.studentsService.deleteStudent(id);
  }
}
