import { ApiProperty } from "@nestjs/swagger";
import { IsInt, IsNotEmpty } from "class-validator";

export class SetGroupForStudentDto {
  @ApiProperty({ description: "Group id", example: 1 })
  @IsNotEmpty()
  @IsInt()
  groupId: number;
}
