// import { PartialType } from '@nestjs/mapped-types';
// import { CreateStudentDto } from './CreateStudent.dto';
import { ApiProperty } from "@nestjs/swagger";
import { IsEmail, IsInt, IsOptional } from "class-validator";

// export class UpdateStudentDto extends PartialType(CreateStudentDto) {
// }

export class UpdateStudentDto {
  @IsOptional()
  @ApiProperty({ description: "Student name", example: "Bill" })
  name?: string;

  @ApiProperty({ description: "Student surname", example: "Gates" })
  @IsOptional()
  surname?: string;

  @ApiProperty({ description: "Student email", example: "gates@outlook.com" })
  @IsOptional()
  @IsEmail()
  email?: string;

  @ApiProperty({ description: "Student age", example: "21" })
  @IsOptional()
  @IsInt()
  age?: number;
}
