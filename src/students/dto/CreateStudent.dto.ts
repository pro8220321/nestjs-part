import { ApiProperty } from "@nestjs/swagger";
import { IsEmail, IsInt, IsNotEmpty } from "class-validator";

export class CreateStudentDto {
  @IsNotEmpty()
  @ApiProperty({ description: "Student name", example: "Bill" })
  name: string;
  @IsNotEmpty()
  @ApiProperty({ description: "Student surname", example: "Gates" })
  surname: string;
  @ApiProperty({ description: "Student email", example: "gates@outlook.com" })
  @IsEmail()
  email: string;
  @ApiProperty({ description: "Student age", example: "21" })
  @IsInt()
  age: number;
}
