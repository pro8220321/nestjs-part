//
import { ApiProperty } from "@nestjs/swagger";

export class StudentResponse {
  @ApiProperty({
    description: "Student id",
    example: "1",
  })
  id: string;

  @ApiProperty({
    description: "Student name",
    example: "Bill",
  })
  name: string;

  @ApiProperty({
    description: "Student surname",
    example: "Gates",
  })
  surname: string;

  @ApiProperty({
    description: "Student email",
    example: "gates@outlook.com",
  })
  email: string;

  @ApiProperty({
    description: "Student's age",
    example: 21,
  })
  age: number;

  @ApiProperty({
    description: "Student's group",
    example: null,
  })
  groupName: string | null;
}

export class StudentCreateResponse {
  @ApiProperty({
    description: "Student name",
    example: "Bill",
  })
  name: string;

  @ApiProperty({
    description: "Student surname",
    example: "Gates",
  })
  surname: string;

  @ApiProperty({
    description: "Student email",
    example: "gates@outlook.com",
  })
  email: string;

  @ApiProperty({
    description: "Student age",
    example: "21",
  })
  age: number;

  @ApiProperty({
    description: "Student image",
    example: null,
  })
  imagePath: string;

  @ApiProperty({
    description: "Student group id",
    example: null,
  })
  groupId: number | null;

  @ApiProperty({
    description: "Student id",
    example: "1",
  })
  id: string;

  @ApiProperty({
    description: "Date student was created",
    example: "06:53:15.498844+00",
  })
  createdAd: Date;

  @ApiProperty({
    description: "Date student was updated",
    example: "06:53:15.498844+00",
  })
  updateAd: Date;
}
