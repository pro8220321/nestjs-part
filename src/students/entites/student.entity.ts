import { Group } from "../../groups/entites/group.entity";
import { CoreEntity } from "../../enteties/core.entety";
import { Column, Entity, Index, JoinColumn, ManyToOne, OneToMany } from "typeorm";
import { Mark } from "../../marks/entites/mark.entity";
import { ApiProperty } from "@nestjs/swagger";

@Entity({ name: "students" })
export class Student extends CoreEntity {
  @ApiProperty({
    description: "Student name",
    example: "Bill",
  })
  @Column({
    type: "varchar",
    nullable: false,
  })
  @Index()
  name: string;

  @ApiProperty({
    description: "Student surname",
    example: "Gates",
  })
  @Column({
    type: "varchar",
    nullable: false,
  })
  surname: string;

  @ApiProperty({
    description: "Student email",
    example: "gates@outlook.com",
  })
  @Column({
    type: "varchar",
    nullable: false,
  })
  email: string;

  @ApiProperty({
    description: "Student's age",
    example: "21",
  })
  @Column({
    type: "numeric",
    nullable: true,
  })
  age: number;

  @ApiProperty({
    description: "Student's image",
    example: null,
  })
  @Column({
    type: "varchar",
    nullable: true,
  })
  imagePath: string;

  @ManyToOne(() => Group, group => group.students, {
    nullable: false,
    eager: false,
    onDelete: "SET NULL",
  })
  @JoinColumn({ name: "group_id" })
  group: Group;

  @ApiProperty({
    description: "Student's group id",
    example: null,
  })
  @Column({
    type: "integer",
    nullable: true,
    name: "group_id",
  })
  groupId: number | null;

  @OneToMany(() => Mark, mark => mark.student)
  marks: Mark[];
}
