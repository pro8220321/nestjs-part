import { HttpException, HttpStatus } from "@nestjs/common";

export class ExceptionsHendler extends HttpException {
  constructor(msg: string, status: HttpStatus) {
    super(msg, status);
  }
}
