import { Body, Controller, HttpCode, HttpStatus, Post } from "@nestjs/common";
import { AuthService } from "./auth.service";
import { SignRequestDto } from "./dto/sign.request.dto";
import { SignResponseDto } from "./dto/sign.response.dto";
import { ResetPasswordRequestDto } from "./dto/reset-password.request.dto";
import { ResetPasswordWithTokenRequestDto } from "./dto/reset-password-with-token.request.dto";
import { CreateLectorDto } from "src/lectors/dto/CreateLector.dto";
import { ApiInternalServerErrorResponse, ApiOkResponse, ApiOperation, ApiTags, ApiUnauthorizedResponse } from "@nestjs/swagger";
import { ResetPasswordResponseDto } from "./dto/reset-password.response.dto";
@ApiTags("Auth")
@Controller("auth")
export class AuthController {
  constructor(private authService: AuthService) {}

  @ApiOperation({ summary: "Sign in" })
  @ApiOkResponse({
    type: [SignResponseDto],
    description: "Access token",
  })
  @ApiUnauthorizedResponse({
    description: "Unauthorized exception",
  })
  @ApiInternalServerErrorResponse({
    description: "Internal Server Error",
  })
  @Post("sign-in")
  public signIn(@Body() signInDto: SignRequestDto): Promise<SignResponseDto> {
    return this.authService.signIn(signInDto.email, signInDto.password);
  }

  @ApiOperation({ summary: "Sign up" })
  @ApiOkResponse({
    type: [SignResponseDto],
    description: "Access token",
  })
  @ApiInternalServerErrorResponse({
    description: "Internal Server Error",
  })
  @Post("sign-up")
  public signUp(@Body() createLectorDto: CreateLectorDto): Promise<SignResponseDto> {
    return this.authService.signUp(createLectorDto);
  }

  @ApiOperation({ summary: "Reset password request" })
  @ApiOkResponse({
    type: [ResetPasswordResponseDto],
    description: "Reset token",
  })
  @HttpCode(HttpStatus.OK)
  @ApiInternalServerErrorResponse({
    description: "Internal Server Error",
  })
  @Post("reset-password-request")
  public resetPasswordRequest(@Body() resetPasswordDto: ResetPasswordRequestDto) {
    return this.authService.resetPasswordRequest(resetPasswordDto.email);
  }

  @ApiOperation({ summary: "Reset password" })
  @ApiOkResponse({
    description: "Reset password",
  })
  @ApiInternalServerErrorResponse({
    description: "Internal Server Error",
  })
  @Post("reset-password")
  public resetPassword(@Body() resetPasswordDto: ResetPasswordWithTokenRequestDto) {
    return this.authService.resetPassword(resetPasswordDto);
  }
}
