import { IsNotEmpty, IsString } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";

export class SignResponseDto {
  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    description: "youre token",
    example: "wrsfhbdgwgeasrdshn eszghjetydgf",
  })
  accessToken: string;
}
