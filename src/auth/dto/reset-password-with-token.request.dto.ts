import { IsEmail, IsNotEmpty, IsString } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";

export class ResetPasswordWithTokenRequestDto {
  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    description: "email",
    example: "someemail@gmail.com",
  })
  @IsEmail()
  email: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    description: "token",
    example: "wrsfhbdgwgeasrdshn eszghjetydgf",
  })
  token: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    description: "old password",
    example: "old password",
  })
  oldPassword: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    description: "new password",
    example: "new password",
  })
  newPassword: string;
}
