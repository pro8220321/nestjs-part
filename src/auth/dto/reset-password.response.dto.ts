import { IsNotEmpty, IsString } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";

export class ResetPasswordResponseDto {
  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    description: "name",
    example: "Bill",
  })
  name: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    description: "token",
    example: "wrsfhbdgwgeasrdshn eszghjetydgf",
  })
  token: string;
}
