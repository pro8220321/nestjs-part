import { IsEmail, IsNotEmpty, IsString } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";

export class ResetPasswordRequestDto {
  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    description: "email",
    example: "someemail@gmail.com",
  })
  @IsEmail()
  email: string;
}
