import { IsEmail, IsNotEmpty, IsString } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";

export class SignRequestDto {
  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    description: "email",
    example: "someemail@gmail.com",
  })
  @IsEmail()
  email: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    description: "password",
    example: "some password",
  })
  password: string;
}
