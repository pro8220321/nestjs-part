import { Injectable, UnauthorizedException, BadRequestException } from "@nestjs/common";
import { JwtService } from "@nestjs/jwt";
import { ResetTokenInterface } from "src/reset-token/interfaces/reset-token.interface";
import { ResetTokenService } from "src/reset-token/reset-token.service";
import { UsersService } from "src/users/users.service";
import { ResetPasswordWithTokenRequestDto } from "./dto/reset-password-with-token.request.dto";
import { SignResponseDto } from "./dto/sign.response.dto";
import { LectorsService } from "src/lectors/lectors.service";
import { CreateLectorDto } from "src/lectors/dto/CreateLector.dto";
import * as bcrypt from "bcrypt";

@Injectable()
export class AuthService {
  constructor(
    private userService: UsersService,
    private jwtService: JwtService,
    private resetTokenService: ResetTokenService,
    private lectorService: LectorsService,
  ) {}

  public async signUp(createLectorDto: CreateLectorDto) {
    const user = await this.lectorService.createLector(createLectorDto);
    const payload = { sub: user.id, username: user.name };
    return {
      accessToken: await this.jwtService.signAsync(payload),
    };
  }

  public async resetPasswordRequest(email: string): Promise<ResetTokenInterface> {
    const user = await this.lectorService.getLectorByEmail(email);
    if (!user) {
      throw new BadRequestException(`Cannot generate token for reset password request because email ${email} is not found`);
    }

    return await this.resetTokenService.generateResetToken(email);
  }

  public async resetPassword(resetPasswordWithTokenRequestDto: ResetPasswordWithTokenRequestDto) {
    const { token, email, oldPassword, newPassword } = resetPasswordWithTokenRequestDto;
    const resetPasswordRequest = await this.resetTokenService.getResetToken(token);
    if (!resetPasswordRequest) {
      throw new BadRequestException(`There is no request password request for user: ${email}`);
    }

    const user = await this.lectorService.getLectorByEmail(email);
    if (!user) {
      throw new BadRequestException(`User is not found`);
    }
    const isValid = await bcrypt.compare(oldPassword, user.password);
    if (!isValid) {
      throw new BadRequestException(`Old password is incorrect`);
    }

    const newHashPassword = await bcrypt.hash(newPassword, Number(bcrypt.genSalt(10)));

    await this.lectorService.setNewPassword(user.id, newHashPassword);
    await this.resetTokenService.removeResetToken(token);
  }

  public async signIn(email: string, pass: string): Promise<SignResponseDto> {
    const user = await this.lectorService.getLectorByEmail(email);
    const isValid = await bcrypt.compare(pass, user?.password);
    if (!isValid) {
      throw new UnauthorizedException();
    }
    const payload = { sub: user.id, username: user.email };

    return {
      accessToken: await this.jwtService.signAsync(payload),
    };
  }
}
