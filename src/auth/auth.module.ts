import { Module } from "@nestjs/common";
import { AuthController } from "./auth.controller";
import { AuthService } from "./auth.service";
import { UsersModule } from "src/users/users.module";
import { JwtModule } from "@nestjs/jwt";
import { jwtConstants } from "./auth.constant";
import { ResetTokenModule } from "src/reset-token/reset-token.module";
import { LectorsModule } from "src/lectors/lectors.module";

@Module({
  imports: [
    UsersModule,
    JwtModule.register({
      global: true,
      secret: jwtConstants.secret,
      signOptions: { expiresIn: "60s" },
    }),
    ResetTokenModule,
    LectorsModule,
  ],
  controllers: [AuthController],
  providers: [AuthService],
})
export class AuthModule {}
