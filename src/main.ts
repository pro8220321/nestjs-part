import "dotenv/config";
import { NestFactory } from "@nestjs/core";
import { AppModule } from "./app.module";
import { ValidationPipe } from "@nestjs/common";
import { DocumentBuilder, SwaggerModule } from "@nestjs/swagger";
const port = process.env.SERVER_PORT;

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.enableCors({
    origin: "http://localhost:3000",
    methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
    credentials: true,
  });
  const swaggerOptions = new DocumentBuilder()
    .setTitle("University API")
    .setDescription("This page provides University API documentation")
    .addBearerAuth({
      type: "http",
      scheme: "bearer",
      bearerFormat: "JWT",
    })
    .build();
  const swaggerDocument = SwaggerModule.createDocument(app, swaggerOptions);
  SwaggerModule.setup(`/api/v1/docs`, app, swaggerDocument);

  app.useGlobalPipes(new ValidationPipe());
  await app.listen(port, () => {
    console.log(`Listen to ${port} port`);
  });
}
bootstrap();
