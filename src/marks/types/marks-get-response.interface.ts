import { ApiProperty } from "@nestjs/swagger";

export class StudentMarks {
  @ApiProperty({
    description: "Student mark",
    example: "5",
  })
  mark: string;

  @ApiProperty({
    description: "Student course",
    example: "music",
  })
  course: string;
}

export class CourseMarks {
  @ApiProperty({
    description: "Student mark",
    example: "music",
  })
  course: string;

  @ApiProperty({
    description: "Student lector",
    example: "Bill",
  })
  lector: string;

  @ApiProperty({
    description: "Student",
    example: "George",
  })
  student: string;

  @ApiProperty({
    description: "Student mark",
    example: "5",
  })
  mark: string;
}

export class AddMarksResponse {
  @ApiProperty({
    description: "Student mark",
    example: "5",
  })
  mark: number;

  @ApiProperty({
    description: "Student course (id)",
    example: 1,
  })
  course: number;

  @ApiProperty({
    description: "Student (id)",
    example: 2,
  })
  student: number;

  @ApiProperty({
    description: "Lector (id)",
    example: 6,
  })
  lector: number;

  @ApiProperty({
    description: "Mark id",
    example: 1,
  })
  id: number;

  @ApiProperty({
    description: "Date mark was created",
    example: "06:53:15.498844+00",
  })
  createdAd: Date;

  @ApiProperty({
    description: "Date mark was updated",
    example: "06:53:15.498844+00",
  })
  updateAd: Date;
}
