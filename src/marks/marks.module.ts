import { Module } from "@nestjs/common";
import { MarksController } from "./marks.controller";
import { MarksService } from "./marks.service";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Mark } from "./entites/mark.entity";

@Module({
  imports: [TypeOrmModule.forFeature([Mark])],
  controllers: [MarksController],
  providers: [MarksService],
})
export class MarksModule {}
