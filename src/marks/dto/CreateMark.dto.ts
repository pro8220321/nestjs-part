import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty } from "class-validator";

export class CreateMarksDto {
  @ApiProperty({
    description: "Student mark",
    example: 5,
  })
  @IsNotEmpty()
  mark: number;

  @ApiProperty({
    description: "Student course (id)",
    example: 1,
  })
  @IsNotEmpty()
  course_id: number;

  @ApiProperty({
    description: "Student (id)",
    example: 2,
  })
  @IsNotEmpty()
  student_id: number;

  @ApiProperty({
    description: "Student lector (id)",
    example: 6,
  })
  @IsNotEmpty()
  lector_id: number;
}
