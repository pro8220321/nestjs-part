import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Mark } from "./entites/mark.entity";
import { Repository } from "typeorm";
import { CreateMarksDto } from "./dto/CreateMark.dto";
import { ExceptionsHendler } from "src/exceptions/exceptions";
@Injectable()
export class MarksService {
  constructor(
    @InjectRepository(Mark)
    private readonly markReposetory: Repository<Mark>,
  ) {}

  async addMark(createMarksDto: CreateMarksDto): Promise<CreateMarksDto> {
    return await this.markReposetory.save(createMarksDto);
  }

  async getAllStudentsMarks(id: string): Promise<Mark[]> {
    const marks = await this.markReposetory
      .createQueryBuilder("marks")
      .leftJoin("marks.course", "course")
      .addSelect('course.name as "course_name"')
      .where("marks.student_id = :id", { id })
      .select(["marks.mark as mark", "course.name as course"])
      .getRawMany();

    if (marks.length === 0) {
      throw new ExceptionsHendler("No marks for this student", 404);
    }

    return marks;
  }

  async getAllMarksOfCourse(id: string): Promise<Mark[]> {
    const marks = await this.markReposetory
      .createQueryBuilder("marks")
      .leftJoin("marks.course", "course")
      .addSelect('course.name as "course"')
      .leftJoin("marks.lector", "lector")
      .addSelect('lector.name as "lector"')
      .leftJoin("marks.student", "student")
      .addSelect('student.name as "student"')
      .where("course.id = :id", { id })
      .select(["course.name as course", "lector.name as lector", "student.name as student", "marks.mark as mark"])
      .getRawMany();

    if (marks.length === 0) {
      throw new ExceptionsHendler("No marks on this course", 404);
    }

    return marks;
  }
}
