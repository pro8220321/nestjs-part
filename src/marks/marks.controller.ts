import { Body, Controller, Get, Post, Query, UseGuards } from "@nestjs/common";
import { MarksService } from "./marks.service";
import { CreateMarksDto } from "./dto/CreateMark.dto";
import { ExceptionsHendler } from "src/exceptions/exceptions";
import {
  ApiBearerAuth,
  ApiExtraModels,
  ApiInternalServerErrorResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiQuery,
  ApiTags,
  ApiUnauthorizedResponse,
  refs,
} from "@nestjs/swagger";
import { AddMarksResponse, CourseMarks, StudentMarks } from "./types/marks-get-response.interface";
import { AuthGuard } from "src/auth/auth.guards";

@ApiTags("Marks")
@Controller("api/v1/marks")
export class MarksController {
  constructor(private markService: MarksService) {}

  @Get()
  @ApiBearerAuth()
  @UseGuards(AuthGuard)
  @ApiOperation({ summary: "Get all marks" })
  @ApiExtraModels(CourseMarks, StudentMarks)
  @ApiQuery({
    name: "student_id",
    type: String,
    required: false,
    description: "ID of the student to retrieve marks for.",
  })
  @ApiQuery({
    name: "course_id",
    type: String,
    required: false,
    description: "ID of the course to retrieve marks for.",
  })
  @ApiOkResponse({
    schema: { anyOf: refs(CourseMarks, StudentMarks) },
  })
  @ApiNotFoundResponse({ description: "No marks on this course or student" })
  @ApiInternalServerErrorResponse({
    description: "Internal Server Error",
  })
  @ApiUnauthorizedResponse({
    description: "Unauthorized exception",
  })
  getMarks(@Query("student_id") student_id: string, @Query("course_id") course_id: string) {
    if (student_id) {
      return this.markService.getAllStudentsMarks(student_id);
    } else if (course_id) {
      return this.markService.getAllMarksOfCourse(course_id);
    } else {
      throw new ExceptionsHendler("Missing parameters", 400);
    }
  }

  @Post("")
  @ApiBearerAuth()
  @UseGuards(AuthGuard)
  @ApiOperation({ summary: "Add mark" })
  @ApiOkResponse({
    description: "Add mark",
    type: AddMarksResponse,
  })
  @ApiInternalServerErrorResponse({
    description: "Internal Server Error",
  })
  @ApiUnauthorizedResponse({
    description: "Unauthorized exception",
  })
  addMark(@Body() createMarksDto: CreateMarksDto) {
    return this.markService.addMark(createMarksDto);
  }
}
