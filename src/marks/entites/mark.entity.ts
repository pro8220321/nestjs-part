import { Column, Entity, JoinColumn, ManyToOne } from "typeorm";
import { CoreEntity } from "../../enteties/core.entety";
import { Course } from "../../courses/entites/course.entity";
import { Student } from "../../students/entites/student.entity";
import { Lector } from "../../lectors/entites/lector.entity";

@Entity({ name: "marks" })
export class Mark extends CoreEntity {
  @Column({
    type: "numeric",
    nullable: false,
  })
  mark: number;

  @ManyToOne(() => Course, course => course.marks)
  @JoinColumn({ name: "course_id" })
  course: Course;

  @ManyToOne(() => Student, student => student.marks)
  @JoinColumn({ name: "student_id" })
  student: Student;

  @ManyToOne(() => Lector, lector => lector.marks)
  @JoinColumn({ name: "lector_id" })
  lector: Lector;
}
