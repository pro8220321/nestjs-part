import { Module } from "@nestjs/common";
import { AppController } from "./app.controller";
import { AppService } from "./app.service";
import { StudentsModule } from "./students/students.module";
import { TypeOrmModule } from "@nestjs/typeorm";
import { GroupsModule } from "./groups/groups.module";
import { LectorsModule } from "./lectors/lectors.module";
import { CoursesModule } from "./courses/courses.module";
import { LectorcourseModule } from "./lectorcourse/lectorcourse.module";
import { MarksModule } from "./marks/marks.module";
import { AuthModule } from "./auth/auth.module";
import { UsersModule } from "./users/users.module";
import { ResetTokenModule } from "./reset-token/reset-token.module";

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: "postgres",
      host: process.env.DATABASE_HOST,
      port: Number(process.env.DATABASE_PORT),
      username: process.env.DATABASE_USER,
      password: process.env.DATABASE_PASSWORD,
      database: process.env.DATABASE_NAME,
      synchronize: false,
      autoLoadEntities: true,
      migrationsTableName: "migrations",
      migrationsRun: true,
      logging: true,
    }),
    StudentsModule,
    GroupsModule,
    LectorsModule,
    CoursesModule,
    LectorcourseModule,
    MarksModule,
    AuthModule,
    UsersModule,
    ResetTokenModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
