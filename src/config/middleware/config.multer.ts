import { diskStorage } from "multer";

const multerConfig = {
  storage: diskStorage({
    destination: "./src/temp",
    filename: (req, file, cb) => {
      const name = file.originalname.split(".")[0];
      const fileExtension = file.originalname.split(".")[1];
      const newFileName = name.split(" ").join("_") + "_" + Date.now() + "." + fileExtension;
      cb(null, newFileName);
    },
  }),
};

export default multerConfig;
