import "dotenv/config";
import { DataSource } from "typeorm";

const AppDataSource = new DataSource({
  type: "postgres",
  host: process.env.DATABASE_HOST,
  port: Number(process.env.DATABASE_PORT),
  username: process.env.DATABASE_USER,
  password: process.env.DATABASE_PASSWORD,
  database: process.env.DATABASE_NAME,
  entities: ["src/**/entites/*.entity.{js,ts}"],
  migrations: ["src/database/migrations/*.{js,ts}"],
  migrationsTableName: "migrations",
  migrationsRun: true,
  logging: true,
});

// entities: ['../**/entites/*.entity.{js,ts}'],
// migrations: ['src/database/migrations/*.{js,ts}'],

AppDataSource.initialize()
  .then(() => {
    console.log("Data Source has been initialized!");
  })
  .catch(err => {
    console.error("Error during Data Source initialization", err);
  });
export default AppDataSource;
