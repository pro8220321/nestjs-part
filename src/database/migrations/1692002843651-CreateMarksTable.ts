import { MigrationInterface, QueryRunner } from "typeorm";

export class CreateMarksTable1692002843651 implements MigrationInterface {
  name = "CreateMarksTable1692002843651";

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "students" DROP CONSTRAINT "FK_b9f6fcd8a397ee5b503191dd7c3"`);
    await queryRunner.query(`ALTER TABLE "lector_courses" DROP CONSTRAINT "FK_b18b7a01e613c49689c096cc917"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_b18b7a01e613c49689c096cc91"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_19e2d32fa072dc64a25a572948"`);
    await queryRunner.query(
      `CREATE TABLE "marks" ("id" SERIAL NOT NULL, "created_at" TIME WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIME WITH TIME ZONE NOT NULL DEFAULT now(), "mark" numeric NOT NULL, "course_id" integer, "student_id" integer, "lector_id" integer, CONSTRAINT "PK_051deeb008f7449216d568872c6" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(`ALTER TABLE "lector_courses" DROP COLUMN "id"`);
    await queryRunner.query(`ALTER TABLE "lector_courses" DROP COLUMN "created_at"`);
    await queryRunner.query(`ALTER TABLE "lector_courses" DROP COLUMN "updated_at"`);
    await queryRunner.query(`ALTER TABLE "lector_courses" ADD "id" SERIAL NOT NULL`);
    await queryRunner.query(`ALTER TABLE "lector_courses" DROP CONSTRAINT "PK_f3e29ea637b59288db4ed7479ab"`);
    await queryRunner.query(`ALTER TABLE "lector_courses" ADD CONSTRAINT "PK_9383e53ffe85f40788d190a1d6e" PRIMARY KEY ("lector_id", "course_id", "id")`);
    await queryRunner.query(`ALTER TABLE "lector_courses" ADD "created_at" TIME WITH TIME ZONE NOT NULL DEFAULT now()`);
    await queryRunner.query(`ALTER TABLE "lector_courses" ADD "updated_at" TIME WITH TIME ZONE NOT NULL DEFAULT now()`);
    await queryRunner.query(`ALTER TABLE "lector_courses" DROP CONSTRAINT "PK_9383e53ffe85f40788d190a1d6e"`);
    await queryRunner.query(`ALTER TABLE "lector_courses" ADD CONSTRAINT "PK_f3e29ea637b59288db4ed7479ab" PRIMARY KEY ("lector_id", "course_id")`);
    await queryRunner.query(`CREATE INDEX "IDX_b18b7a01e613c49689c096cc91" ON "lector_courses" ("lector_id") `);
    await queryRunner.query(`CREATE INDEX "IDX_19e2d32fa072dc64a25a572948" ON "lector_courses" ("course_id") `);
    await queryRunner.query(
      `ALTER TABLE "marks" ADD CONSTRAINT "FK_3e39a10631f1c639777a2b99cb8" FOREIGN KEY ("course_id") REFERENCES "courses"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "marks" ADD CONSTRAINT "FK_5226e1592e6291dbe7a07640346" FOREIGN KEY ("student_id") REFERENCES "students"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "marks" ADD CONSTRAINT "FK_69b5af3347a46bb74ccc3c6f65c" FOREIGN KEY ("lector_id") REFERENCES "lectors"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "students" ADD CONSTRAINT "FK_b9f6fcd8a397ee5b503191dd7c3" FOREIGN KEY ("group_id") REFERENCES "groups"("id") ON DELETE SET NULL ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "lector_courses" ADD CONSTRAINT "FK_b18b7a01e613c49689c096cc917" FOREIGN KEY ("lector_id") REFERENCES "lectors"("id") ON DELETE CASCADE ON UPDATE CASCADE`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "lector_courses" DROP CONSTRAINT "FK_b18b7a01e613c49689c096cc917"`);
    await queryRunner.query(`ALTER TABLE "students" DROP CONSTRAINT "FK_b9f6fcd8a397ee5b503191dd7c3"`);
    await queryRunner.query(`ALTER TABLE "marks" DROP CONSTRAINT "FK_69b5af3347a46bb74ccc3c6f65c"`);
    await queryRunner.query(`ALTER TABLE "marks" DROP CONSTRAINT "FK_5226e1592e6291dbe7a07640346"`);
    await queryRunner.query(`ALTER TABLE "marks" DROP CONSTRAINT "FK_3e39a10631f1c639777a2b99cb8"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_19e2d32fa072dc64a25a572948"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_b18b7a01e613c49689c096cc91"`);
    await queryRunner.query(`ALTER TABLE "lector_courses" DROP CONSTRAINT "PK_f3e29ea637b59288db4ed7479ab"`);
    await queryRunner.query(`ALTER TABLE "lector_courses" ADD CONSTRAINT "PK_9383e53ffe85f40788d190a1d6e" PRIMARY KEY ("lector_id", "course_id", "id")`);
    await queryRunner.query(`ALTER TABLE "lector_courses" DROP COLUMN "updated_at"`);
    await queryRunner.query(`ALTER TABLE "lector_courses" DROP COLUMN "created_at"`);
    await queryRunner.query(`ALTER TABLE "lector_courses" DROP CONSTRAINT "PK_9383e53ffe85f40788d190a1d6e"`);
    await queryRunner.query(`ALTER TABLE "lector_courses" ADD CONSTRAINT "PK_f3e29ea637b59288db4ed7479ab" PRIMARY KEY ("lector_id", "course_id")`);
    await queryRunner.query(`ALTER TABLE "lector_courses" DROP COLUMN "id"`);
    await queryRunner.query(`ALTER TABLE "lector_courses" ADD "updated_at" TIME WITH TIME ZONE NOT NULL DEFAULT now()`);
    await queryRunner.query(`ALTER TABLE "lector_courses" ADD "created_at" TIME WITH TIME ZONE NOT NULL DEFAULT now()`);
    await queryRunner.query(`ALTER TABLE "lector_courses" ADD "id" SERIAL NOT NULL`);
    await queryRunner.query(`DROP TABLE "marks"`);
    await queryRunner.query(`CREATE INDEX "IDX_19e2d32fa072dc64a25a572948" ON "lector_courses" ("course_id") `);
    await queryRunner.query(`CREATE INDEX "IDX_b18b7a01e613c49689c096cc91" ON "lector_courses" ("lector_id") `);
    await queryRunner.query(
      `ALTER TABLE "lector_courses" ADD CONSTRAINT "FK_b18b7a01e613c49689c096cc917" FOREIGN KEY ("lector_id") REFERENCES "lectors"("id") ON DELETE CASCADE ON UPDATE CASCADE`,
    );
    await queryRunner.query(
      `ALTER TABLE "students" ADD CONSTRAINT "FK_b9f6fcd8a397ee5b503191dd7c3" FOREIGN KEY ("group_id") REFERENCES "groups"("id") ON DELETE SET NULL ON UPDATE NO ACTION`,
    );
  }
}
