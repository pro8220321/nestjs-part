import { MigrationInterface, QueryRunner } from "typeorm";

export class CreateLectorAndCoursesTable1692001497465 implements MigrationInterface {
  name = "CreateLectorAndCoursesTable1692001497465";

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "students" DROP CONSTRAINT "FK_b9f6fcd8a397ee5b503191dd7c3"`);
    await queryRunner.query(
      `CREATE TABLE "lectors" ("id" SERIAL NOT NULL, "created_at" TIME WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIME WITH TIME ZONE NOT NULL DEFAULT now(), "name" character varying NOT NULL, "email" character varying NOT NULL, "password" character varying NOT NULL, CONSTRAINT "PK_87eda9bf8c85d84a6b18dfc4991" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "courses" ("id" SERIAL NOT NULL, "created_at" TIME WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIME WITH TIME ZONE NOT NULL DEFAULT now(), "name" character varying NOT NULL, "description" character varying NOT NULL, "hours" numeric NOT NULL, CONSTRAINT "PK_3f70a487cc718ad8eda4e6d58c9" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "lector_courses" ("id" SERIAL NOT NULL, "created_at" TIME WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIME WITH TIME ZONE NOT NULL DEFAULT now(), "lector_id" integer NOT NULL, "course_id" integer NOT NULL, CONSTRAINT "PK_9383e53ffe85f40788d190a1d6e" PRIMARY KEY ("id", "lector_id", "course_id"))`,
    );
    await queryRunner.query(`ALTER TABLE "lector_courses" DROP CONSTRAINT "PK_9383e53ffe85f40788d190a1d6e"`);
    await queryRunner.query(`ALTER TABLE "lector_courses" ADD CONSTRAINT "PK_f3e29ea637b59288db4ed7479ab" PRIMARY KEY ("lector_id", "course_id")`);
    await queryRunner.query(`ALTER TABLE "lector_courses" DROP COLUMN "id"`);
    await queryRunner.query(`ALTER TABLE "lector_courses" DROP COLUMN "created_at"`);
    await queryRunner.query(`ALTER TABLE "lector_courses" DROP COLUMN "updated_at"`);
    await queryRunner.query(`ALTER TABLE "lector_courses" ADD "id" SERIAL NOT NULL`);
    await queryRunner.query(`ALTER TABLE "lector_courses" DROP CONSTRAINT "PK_f3e29ea637b59288db4ed7479ab"`);
    await queryRunner.query(`ALTER TABLE "lector_courses" ADD CONSTRAINT "PK_9383e53ffe85f40788d190a1d6e" PRIMARY KEY ("lector_id", "course_id", "id")`);
    await queryRunner.query(`ALTER TABLE "lector_courses" ADD "created_at" TIME WITH TIME ZONE NOT NULL DEFAULT now()`);
    await queryRunner.query(`ALTER TABLE "lector_courses" ADD "updated_at" TIME WITH TIME ZONE NOT NULL DEFAULT now()`);
    await queryRunner.query(`ALTER TABLE "lector_courses" DROP CONSTRAINT "PK_9383e53ffe85f40788d190a1d6e"`);
    await queryRunner.query(`ALTER TABLE "lector_courses" ADD CONSTRAINT "PK_f3e29ea637b59288db4ed7479ab" PRIMARY KEY ("lector_id", "course_id")`);
    await queryRunner.query(`CREATE INDEX "IDX_b18b7a01e613c49689c096cc91" ON "lector_courses" ("lector_id") `);
    await queryRunner.query(`CREATE INDEX "IDX_19e2d32fa072dc64a25a572948" ON "lector_courses" ("course_id") `);
    await queryRunner.query(
      `ALTER TABLE "students" ADD CONSTRAINT "FK_b9f6fcd8a397ee5b503191dd7c3" FOREIGN KEY ("group_id") REFERENCES "groups"("id") ON DELETE SET NULL ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "lector_courses" ADD CONSTRAINT "FK_19e2d32fa072dc64a25a572948d" FOREIGN KEY ("course_id") REFERENCES "courses"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "lector_courses" ADD CONSTRAINT "FK_b18b7a01e613c49689c096cc917" FOREIGN KEY ("lector_id") REFERENCES "lectors"("id") ON DELETE CASCADE ON UPDATE CASCADE`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "lector_courses" DROP CONSTRAINT "FK_b18b7a01e613c49689c096cc917"`);
    await queryRunner.query(`ALTER TABLE "lector_courses" DROP CONSTRAINT "FK_19e2d32fa072dc64a25a572948d"`);
    await queryRunner.query(`ALTER TABLE "students" DROP CONSTRAINT "FK_b9f6fcd8a397ee5b503191dd7c3"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_19e2d32fa072dc64a25a572948"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_b18b7a01e613c49689c096cc91"`);
    await queryRunner.query(`ALTER TABLE "lector_courses" DROP CONSTRAINT "PK_f3e29ea637b59288db4ed7479ab"`);
    await queryRunner.query(`ALTER TABLE "lector_courses" ADD CONSTRAINT "PK_9383e53ffe85f40788d190a1d6e" PRIMARY KEY ("lector_id", "course_id", "id")`);
    await queryRunner.query(`ALTER TABLE "lector_courses" DROP COLUMN "updated_at"`);
    await queryRunner.query(`ALTER TABLE "lector_courses" DROP COLUMN "created_at"`);
    await queryRunner.query(`ALTER TABLE "lector_courses" DROP CONSTRAINT "PK_9383e53ffe85f40788d190a1d6e"`);
    await queryRunner.query(`ALTER TABLE "lector_courses" ADD CONSTRAINT "PK_f3e29ea637b59288db4ed7479ab" PRIMARY KEY ("lector_id", "course_id")`);
    await queryRunner.query(`ALTER TABLE "lector_courses" DROP COLUMN "id"`);
    await queryRunner.query(`ALTER TABLE "lector_courses" ADD "updated_at" TIME WITH TIME ZONE NOT NULL DEFAULT now()`);
    await queryRunner.query(`ALTER TABLE "lector_courses" ADD "created_at" TIME WITH TIME ZONE NOT NULL DEFAULT now()`);
    await queryRunner.query(`ALTER TABLE "lector_courses" ADD "id" SERIAL NOT NULL`);
    await queryRunner.query(`ALTER TABLE "lector_courses" DROP CONSTRAINT "PK_f3e29ea637b59288db4ed7479ab"`);
    await queryRunner.query(`ALTER TABLE "lector_courses" ADD CONSTRAINT "PK_9383e53ffe85f40788d190a1d6e" PRIMARY KEY ("id", "lector_id", "course_id")`);
    await queryRunner.query(`DROP TABLE "lector_courses"`);
    await queryRunner.query(`DROP TABLE "courses"`);
    await queryRunner.query(`DROP TABLE "lectors"`);
    await queryRunner.query(
      `ALTER TABLE "students" ADD CONSTRAINT "FK_b9f6fcd8a397ee5b503191dd7c3" FOREIGN KEY ("group_id") REFERENCES "groups"("id") ON DELETE SET NULL ON UPDATE NO ACTION`,
    );
  }
}
