import { ApiProperty } from "@nestjs/swagger";
import { StudentCreateResponse } from "src/students/types/student-get-response.interface";

export class GroupResonse {
  @ApiProperty({
    description: "Group id",
    example: "1",
  })
  id: string;

  @ApiProperty({
    description: "Date Group was created",
    example: "06:53:15.498844+00",
  })
  createdAd: Date;

  @ApiProperty({
    description: "Date Group was updated",
    example: "06:53:15.498844+00",
  })
  updateAd: Date;

  @ApiProperty({
    description: "Group name",
    example: "Math",
  })
  name: string;

  @ApiProperty({
    description: "Group stundents",
    type: [StudentCreateResponse],
  })
  students: StudentCreateResponse[];
}

export class GroupCreateResonse {
  @ApiProperty({
    description: "Group name",
    example: "Math",
  })
  name: string;

  @ApiProperty({
    description: "Group id",
    example: "1",
  })
  id: string;

  @ApiProperty({
    description: "Date Group was created",
    example: "06:53:15.498844+00",
  })
  createdAd: Date;

  @ApiProperty({
    description: "Date Group was updated",
    example: "06:53:15.498844+00",
  })
  updateAd: Date;
}
