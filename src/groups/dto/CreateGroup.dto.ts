import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty } from "class-validator";

export class CreateGroupDto {
  @ApiProperty({
    description: "Group name",
    example: "Math",
  })
  @IsNotEmpty()
  name: string;
}
