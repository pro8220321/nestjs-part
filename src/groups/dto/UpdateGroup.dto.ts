import { ApiProperty } from "@nestjs/swagger";
export class UpdateGroupDto {
  @ApiProperty({
    description: "Group name",
    example: "Math",
  })
  name?: string;
}
