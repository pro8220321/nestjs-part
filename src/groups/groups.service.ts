import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Group } from "./entites/group.entity";
import { Repository } from "typeorm";
import { CreateGroupDto } from "./dto/CreateGroup.dto";
import { UpdateGroupDto } from "./dto/UpdateGroup.dto";
import { ExceptionsHendler } from "src/exceptions/exceptions";

@Injectable()
export class GroupsService {
  constructor(
    @InjectRepository(Group)
    private readonly groupReposetory: Repository<Group>,
  ) {}

  async getAllGroups(): Promise<Group[]> {
    return await this.groupReposetory.createQueryBuilder("group").leftJoinAndSelect("group.students", "student").getMany();
  }

  async getGroupById(id: string): Promise<Group> {
    const group = await this.groupReposetory.createQueryBuilder("group").leftJoinAndSelect("group.students", "student").where("group.id = :id", { id }).getOne();

    if (!group) {
      throw new ExceptionsHendler("Group with this id not found", 404);
    }

    return group;
  }

  async createGroup(createGroupDto: CreateGroupDto): Promise<CreateGroupDto & Group> {
    return await this.groupReposetory.save(createGroupDto);
  }

  async updateGroup(id: string, updateStudentDto: UpdateGroupDto): Promise<false | Group> {
    const group = await this.getGroupById(id);
    if (!group) {
      throw new ExceptionsHendler("Group with this id not found", 404);
    }
    await this.groupReposetory.update(id, updateStudentDto);

    return await this.getGroupById(id);
  }

  async deleteGroup(id: string) {
    const group = await this.getGroupById(id);
    if (!group) {
      throw new ExceptionsHendler("Group with this id not found", 404);
    }
    await this.groupReposetory.delete(id);
  }
}
