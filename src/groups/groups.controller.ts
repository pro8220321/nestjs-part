import { Body, Controller, Delete, Get, Param, Patch, Post, UseGuards } from "@nestjs/common";
import { CreateGroupDto } from "./dto/CreateGroup.dto";
import { GroupsService } from "./groups.service";
import { UpdateGroupDto } from "./dto/UpdateGroup.dto";
import {
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiInternalServerErrorResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse,
} from "@nestjs/swagger";
import { GroupCreateResonse, GroupResonse } from "./types/group-get-response.interface";
import { AuthGuard } from "src/auth/auth.guards";
@ApiTags("Groups")
@Controller("api/v1/groups")
export class GroupsController {
  constructor(private groupService: GroupsService) {}

  @Get("")
  @ApiBearerAuth()
  @UseGuards(AuthGuard)
  @ApiOperation({ summary: "Get all groups" })
  @ApiOkResponse({
    description: "Get all groups",
    type: [GroupResonse],
  })
  @ApiInternalServerErrorResponse({
    description: "Internal Server Error",
  })
  @ApiUnauthorizedResponse({
    description: "Unauthorized exception",
  })
  async getAllGroups() {
    return await this.groupService.getAllGroups();
  }

  @Get("/:id")
  @ApiBearerAuth()
  @UseGuards(AuthGuard)
  @ApiOperation({ summary: "Get group by id" })
  @ApiOkResponse({
    description: "Get group by id",
    type: GroupResonse,
  })
  @ApiNotFoundResponse({ description: "Group with this id not found" })
  @ApiInternalServerErrorResponse({
    description: "Internal Server Error",
  })
  @ApiUnauthorizedResponse({
    description: "Unauthorized exception",
  })
  async getGroupById(@Param("id") id: string) {
    return await this.groupService.getGroupById(id);
  }

  @Post("")
  @ApiBearerAuth()
  @UseGuards(AuthGuard)
  @ApiOperation({ summary: "Create group" })
  @ApiCreatedResponse({
    description: "Create group",
    type: GroupCreateResonse,
  })
  @ApiInternalServerErrorResponse({
    description: "Internal Server Error",
  })
  @ApiUnauthorizedResponse({
    description: "Unauthorized exception",
  })
  async createGroup(@Body() createGroupDto: CreateGroupDto) {
    return await this.groupService.createGroup(createGroupDto);
  }

  @Patch("/:id")
  @ApiBearerAuth()
  @UseGuards(AuthGuard)
  @ApiOperation({ summary: "Update group" })
  @ApiOkResponse({ description: "Update group", type: GroupResonse })
  @ApiInternalServerErrorResponse({
    description: "Internal Server Error",
  })
  @ApiUnauthorizedResponse({
    description: "Unauthorized exception",
  })
  async updateGroup(@Body() updateGroupDto: UpdateGroupDto, @Param("id") id: string) {
    return await this.groupService.updateGroup(id, updateGroupDto);
  }

  @Delete("/:id")
  @ApiBearerAuth()
  @UseGuards(AuthGuard)
  @ApiOperation({ summary: "Delete group" })
  @ApiOkResponse({ description: "Delete group" })
  @ApiInternalServerErrorResponse({
    description: "Internal Server Error",
  })
  @ApiUnauthorizedResponse({
    description: "Unauthorized exception",
  })
  async deleteGroup(@Param("id") id: string) {
    await this.groupService.deleteGroup(id);
  }
}
