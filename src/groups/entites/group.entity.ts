import { Column, Entity, OneToMany } from "typeorm";
import { CoreEntity } from "../../enteties/core.entety";
import { Student } from "../../students/entites/student.entity";

@Entity({ name: "groups" })
export class Group extends CoreEntity {
  @Column({
    type: "varchar",
    nullable: false,
  })
  name: string;

  @OneToMany(() => Student, student => student.group)
  students: Student[];
}
