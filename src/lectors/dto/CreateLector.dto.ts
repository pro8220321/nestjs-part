import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty } from "class-validator";

export class CreateLectorDto {
  @ApiProperty({
    description: "Lector name",
    example: "Steve",
  })
  @IsNotEmpty()
  name: string;

  @ApiProperty({
    description: "Lector email",
    example: "jobs@icloud.com",
  })
  @IsNotEmpty()
  email: string;

  @ApiProperty({
    description: "Lector password",
    example: "pass",
  })
  @IsNotEmpty()
  password: string;
}
