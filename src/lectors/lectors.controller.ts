import { Body, Controller, Get, Param, Post } from "@nestjs/common";
import { LectorsService } from "./lectors.service";
import { CreateLectorDto } from "./dto/CreateLector.dto";
import { ApiInternalServerErrorResponse, ApiNotFoundResponse, ApiOkResponse, ApiOperation, ApiTags } from "@nestjs/swagger";
import { LectorByIdResponse, LectorResponse, LectorCreateResponse } from "./types/lector-get-response.interface";
@ApiTags("Lectors")
@Controller("api/v1/lectors")
export class LectorsController {
  constructor(private lectorService: LectorsService) {}

  @Get("")
  @ApiOperation({ summary: "Get all lectors" })
  @ApiOkResponse({
    description: "Get all lectors",
    type: [LectorResponse],
  })
  @ApiInternalServerErrorResponse({
    description: "Internal Server Error",
  })
  async getAllLectors() {
    return await this.lectorService.getAllLectors();
  }

  @Get("/:id")
  @ApiOperation({ summary: "Get lector by id" })
  @ApiOkResponse({
    description: "Get lector by id",
    type: LectorByIdResponse,
  })
  @ApiNotFoundResponse({ description: "Lector with this id not found" })
  @ApiInternalServerErrorResponse({
    description: "Internal Server Error",
  })
  async getLectorById(@Param("id") id: string) {
    return await this.lectorService.getLectorById(id);
  }

  @Post("")
  @ApiOperation({ summary: "Create lector" })
  @ApiOkResponse({ description: "Create lector", type: LectorCreateResponse })
  @ApiInternalServerErrorResponse({
    description: "Internal Server Error",
  })
  async createLector(@Body() createLectorDto: CreateLectorDto) {
    return await this.lectorService.createLector(createLectorDto);
  }
}
