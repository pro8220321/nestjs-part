import { Module } from "@nestjs/common";
import { LectorsController } from "./lectors.controller";
import { LectorsService } from "./lectors.service";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Lector } from "./entites/lector.entity";

@Module({
  imports: [TypeOrmModule.forFeature([Lector])],
  controllers: [LectorsController],
  providers: [LectorsService],
  exports: [LectorsService],
})
export class LectorsModule {}
