import { ApiProperty } from "@nestjs/swagger";
import { Mark } from "src/marks/entites/mark.entity";

export class LectorResponse {
  @ApiProperty({
    description: "Lector id",
    example: "1",
  })
  id: string;

  @ApiProperty({
    description: "Lector email",
    example: "Steve",
  })
  name: string;

  @ApiProperty({
    description: "Lector email",
    example: "jobs@icloud.com",
  })
  email: string;

  @ApiProperty({
    description: "Lector email",
    example: "pass",
  })
  password: string;
}

export class LectorByIdResponse {
  @ApiProperty({
    description: "Lector id",
    example: "1",
  })
  id: string;

  @ApiProperty({
    description: "Lector email",
    example: "Steve",
  })
  name: string;

  @ApiProperty({
    description: "Lector email",
    example: "jobs@icloud.com",
  })
  email: string;

  @ApiProperty({
    description: "Lector email",
    example: "pass",
  })
  password: string;

  @ApiProperty({
    description: "Lector student and marks",
    type: [Mark],
  })
  marks: Mark[];
}

export class LectorCreateResponse {
  @ApiProperty({
    description: "Lector email",
    example: "Steve",
  })
  name: string;

  @ApiProperty({
    description: "Lector email",
    example: "jobs@icloud.com",
  })
  email: string;

  @ApiProperty({
    description: "Lector email",
    example: "pass",
  })
  password: string;

  @ApiProperty({
    description: "Lector id",
    example: "1",
  })
  id: string;

  @ApiProperty({
    description: "Date lector was created",
    example: "06:53:15.498844+00",
  })
  createdAd: Date;

  @ApiProperty({
    description: "Date lector was updated",
    example: "06:53:15.498844+00",
  })
  updateAd: Date;
}
