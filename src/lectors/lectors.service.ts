import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { Lector } from "./entites/lector.entity";
import { CreateLectorDto } from "./dto/CreateLector.dto";
import { ExceptionsHendler } from "src/exceptions/exceptions";
import * as bcrypt from "bcrypt";
@Injectable()
export class LectorsService {
  constructor(
    @InjectRepository(Lector)
    private readonly lectorReposetory: Repository<Lector>,
  ) {}

  async getAllLectors(): Promise<Lector[]> {
    return await this.lectorReposetory
      .createQueryBuilder("lector")
      .select(["lector.id as id", "lector.name as name", "lector.email as email", "lector.password as password"])
      .getRawMany();
  }

  async getLectorById(id: string): Promise<Lector> {
    const lector = await this.lectorReposetory
      .createQueryBuilder("lectors")
      .distinctOn(["lectors.id", "course.id", "student.id"]) // get courses and students from marks and select only unique values
      .leftJoinAndSelect("lectors.marks", "mark")
      .leftJoinAndSelect("mark.student", "student")
      .leftJoinAndSelect("mark.course", "course")
      .where("lectors.id = :id", { id })
      .select(["lectors.id", "lectors.name", "lectors.email", "lectors.password", "mark.id", "student", "course"])
      .getOne();

    if (!lector) {
      throw new ExceptionsHendler("Lector with this id not found", 404);
    }

    return lector;
  }

  async createLector(createLectorDto: CreateLectorDto) {
    const hashPass = await bcrypt.hash(createLectorDto.password, Number(bcrypt.genSalt(10)));
    createLectorDto.password = hashPass;
    return await this.lectorReposetory.save(createLectorDto);
  }

  public async getLectorByEmail(email: string) {
    return await this.lectorReposetory.findOne({
      where: {
        email: email,
      },
    });
  }

  public async setNewPassword(id: string, password: string) {
    return await this.lectorReposetory.update(id, { password: password });
  }
}
