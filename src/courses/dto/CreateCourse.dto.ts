import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty } from "class-validator";

export class CreateCourseDto {
  @ApiProperty({
    description: "Name course",
    example: "Harvard",
  })
  @IsNotEmpty()
  name: string;

  @ApiProperty({
    description: "Course description",
    example: "Best university",
  })
  @IsNotEmpty()
  description: string;

  @ApiProperty({
    description: "Course hours",
    example: 100,
  })
  @IsNotEmpty()
  hours: number;
}
