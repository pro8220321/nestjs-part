import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { Course } from "./entites/course.entity";
import { CreateCourseDto } from "./dto/CreateCourse.dto";
import { LectorCourse } from "src/lectorcourse/entites/lector.course.entity";
import { ExceptionsHendler } from "src/exceptions/exceptions";
import { Lector } from "src/lectors/entites/lector.entity";

@Injectable()
export class CoursesService {
  constructor(
    @InjectRepository(Course)
    private readonly courseReposetory: Repository<Course>,
    @InjectRepository(LectorCourse)
    private readonly lectorcourseReposetory: Repository<LectorCourse>,
    @InjectRepository(Lector)
    private readonly lectorReposetory: Repository<Lector>,
  ) {}

  async getAllCourses(): Promise<Course[]> {
    return await this.courseReposetory.find({});
  }

  async createCourse(createCourseDto: CreateCourseDto): Promise<CreateCourseDto & Course> {
    return await this.courseReposetory.save(createCourseDto);
  }

  async addLector(idCourse: string, idLector: string): Promise<LectorCourse> {
    const course = await this.courseReposetory.findOne({
      where: {
        id: idCourse,
      },
    });

    if (!course) {
      throw new ExceptionsHendler("Course with this id not found", 404);
    }

    const lector = await this.lectorReposetory.findOne({
      where: {
        id: idLector,
      },
    });

    if (!lector) {
      throw new ExceptionsHendler("Lector with this id not found", 404);
    }

    await this.lectorcourseReposetory
      .createQueryBuilder("lectorcourse")
      .insert()
      .values([{ lectorId: parseInt(idLector), courseId: parseInt(idCourse) }])
      .execute();

    return await this.lectorcourseReposetory.createQueryBuilder().orderBy("id", "DESC").getOne();
  }
}
