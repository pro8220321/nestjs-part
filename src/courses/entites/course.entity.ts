import { Column, Entity, ManyToMany, OneToMany } from "typeorm";
import { CoreEntity } from "../../enteties/core.entety";
import { Lector } from "../../lectors/entites/lector.entity";
import { Mark } from "../../marks/entites/mark.entity";

@Entity({ name: "courses" })
export class Course extends CoreEntity {
  @Column({
    type: "varchar",
    nullable: false,
  })
  name: string;

  @Column({
    type: "varchar",
    nullable: false,
  })
  description: string;

  @Column({
    type: "numeric",
    nullable: false,
  })
  hours: number;

  @ManyToMany(() => Lector, lector => lector.courses)
  lectors: Lector[];

  @OneToMany(() => Mark, mark => mark.course)
  marks: Mark[];
}
