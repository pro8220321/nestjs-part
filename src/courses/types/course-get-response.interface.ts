import { ApiProperty } from "@nestjs/swagger";

export class CourseResponse {
  @ApiProperty({
    description: "course id",
    example: "1",
  })
  id: string;

  @ApiProperty({
    description: "Date course was created",
    example: "06:53:15.498844+00",
  })
  createdAd: Date;

  @ApiProperty({
    description: "Date course was updated",
    example: "06:53:15.498844+00",
  })
  updateAd: Date;

  @ApiProperty({
    description: "Course name",
    example: "Harvard",
  })
  name: string;

  @ApiProperty({
    description: "Course description",
    example: "Best university",
  })
  description: string;

  @ApiProperty({
    description: "Course hours",
    example: 100,
  })
  hours: number;
}

export class CourseCreateResonse {
  @ApiProperty({
    description: "course id",
    example: "1",
  })
  id: string;

  @ApiProperty({
    description: "Date course was created",
    example: "06:53:15.498844+00",
  })
  createdAd: Date;

  @ApiProperty({
    description: "Date course was updated",
    example: "06:53:15.498844+00",
  })
  updateAd: Date;

  @ApiProperty({
    description: "Lector id",
    example: "1",
  })
  lectorId: string;

  @ApiProperty({
    description: "Lector id",
    example: "1",
  })
  courseId: string;
}
