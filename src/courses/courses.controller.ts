import { Body, Controller, Get, Param, Patch, Post, Query, UseGuards } from "@nestjs/common";
import { CoursesService } from "./courses.service";
import { CreateCourseDto } from "./dto/CreateCourse.dto";
import { ApiBearerAuth, ApiInternalServerErrorResponse, ApiNotFoundResponse, ApiOkResponse, ApiOperation, ApiTags } from "@nestjs/swagger";
import { CourseCreateResonse, CourseResponse } from "./types/course-get-response.interface";
import { AuthGuard } from "src/auth/auth.guards";
@ApiTags("Courses")
@Controller("api/v1/courses")
export class CoursesController {
  constructor(private coursesService: CoursesService) {}

  @Get("")
  @ApiBearerAuth()
  @UseGuards(AuthGuard)
  @ApiOperation({ summary: "Get all courses" })
  @ApiOkResponse({
    description: "Get all courses",
    type: [CourseResponse],
  })
  @ApiInternalServerErrorResponse({
    description: "Internal Server Error",
  })
  async getAllCourses() {
    return await this.coursesService.getAllCourses();
  }

  @Post("")
  @ApiBearerAuth()
  @UseGuards(AuthGuard)
  @ApiOperation({ summary: "Create course" })
  @ApiOkResponse({
    description: "Create course",
    type: CourseResponse,
  })
  @ApiInternalServerErrorResponse({
    description: "Internal Server Error",
  })
  async createCourse(@Body() createCourseDto: CreateCourseDto) {
    return await this.coursesService.createCourse(createCourseDto);
  }

  @Patch("/:id")
  @ApiBearerAuth()
  @UseGuards(AuthGuard)
  @ApiOperation({ summary: "Add lector to course" })
  @ApiOkResponse({
    description: "Add lector to course",
    type: CourseCreateResonse,
  })
  @ApiNotFoundResponse({
    description: "Lector or course with this id not found",
  })
  @ApiInternalServerErrorResponse({
    description: "Internal Server Error",
  })
  async addLector(@Param("id") id: string, @Query("lector_id") lector_id: string) {
    return await this.coursesService.addLector(id, lector_id);
  }
}
