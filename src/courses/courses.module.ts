import { Module } from "@nestjs/common";
import { CoursesService } from "./courses.service";
import { CoursesController } from "./courses.controller";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Course } from "./entites/course.entity";
import { LectorcourseModule } from "src/lectorcourse/lectorcourse.module";
import { LectorCourse } from "src/lectorcourse/entites/lector.course.entity";
import { LectorsModule } from "src/lectors/lectors.module";
import { Lector } from "src/lectors/entites/lector.entity";

@Module({
  imports: [TypeOrmModule.forFeature([Course, LectorCourse, Lector]), LectorcourseModule, LectorsModule],
  providers: [CoursesService],
  controllers: [CoursesController],
})
export class CoursesModule {}
